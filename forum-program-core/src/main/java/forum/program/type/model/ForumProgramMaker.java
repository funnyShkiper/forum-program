package forum.program.type.model;

import org.nuxeo.ecm.core.api.CoreSession;
import org.nuxeo.ecm.core.api.DocumentModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ForumProgramMaker {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private CoreSession session;
    private ForumProgram forumProgram;
    private String forumTitle;

    public ForumProgramMaker() {
    }

    public ForumProgramMaker(CoreSession session, String forumTitle) {
        this.session = session;
        this.forumTitle = forumTitle;
        forumProgram = new ForumProgram();
        initForumProgram(session, forumTitle);
    }

    private void initForumProgram(CoreSession session, String forumTitle) {
        DocumentModel rootFolder = session.getRootDocument();
        List<DocumentModel> list = session.getChildren(rootFolder.getRef());

        DocumentModel defaultDomain = null;
        for (DocumentModel doc : list) {
            if (doc.getType().equals("Domain")) {
                defaultDomain = doc;
                break;
            }
        }

        list.clear();
        list = session.getChildren(defaultDomain.getRef());

        DocumentModel sectionsDoc = null;
        for (DocumentModel doc : list) {
            if (doc.getType().equals("SectionRoot")) {
                sectionsDoc = doc;
                break;
            }
        }

        DocumentModel publishSessionsFolder = findFolderWithPublishDocumentsByForumProgramTitle(session, sectionsDoc, forumTitle);
        list.clear();
        list = session.getChildren(publishSessionsFolder.getRef());

        Set<Section> sections = new HashSet<>();
        for (DocumentModel doc : list) {
            Section section = new Section();
            section.setTitle(doc.getTitle());
            section.setDescription(doc.getPropertyValue("dc:description").toString());
            sections.add(section);
        }
        forumProgram.setTitle(forumTitle);
        forumProgram.setSections(sections);
    }

    private DocumentModel findFolderWithPublishDocumentsByForumProgramTitle(CoreSession session, DocumentModel documentModel, String forumTitle) {
        List<DocumentModel> docs = session.getChildren(documentModel.getRef());
        for (DocumentModel doc : docs) {
            if (doc.getType().equals("Section")) {
                if (doc.getTitle().equals(forumTitle)) {
                    return doc;
                } else {
                    findFolderWithPublishDocumentsByForumProgramTitle(session, doc, forumTitle);
                }
            }
        }
        return null;
    }

    public ForumProgram getForumProgram() {
        return forumProgram;
    }
}
