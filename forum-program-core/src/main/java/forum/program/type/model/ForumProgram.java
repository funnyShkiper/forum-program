package forum.program.type.model;

import java.util.Set;

/**
 * Класс представляющий программу форума, в соответствии с готовыми к публикации сессий форума
 */
public class ForumProgram {

    private String title;
    private String date;
    private String place;
    private Set<Section> sections;

    public ForumProgram() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Set<Section> getSections() {
        return sections;
    }

    public void setSections(Set<Section> sections) {
        this.sections = sections;
    }
}
