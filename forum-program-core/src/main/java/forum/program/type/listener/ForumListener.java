package forum.program.type.listener;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import forum.program.type.model.ForumProgram;
import forum.program.type.model.ForumProgramMaker;
import forum.program.type.model.Section;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuxeo.ecm.automation.AutomationService;
import org.nuxeo.ecm.automation.OperationChain;
import org.nuxeo.ecm.automation.OperationContext;
import org.nuxeo.ecm.automation.OperationException;
import org.nuxeo.ecm.core.api.Blob;
import org.nuxeo.ecm.core.api.CoreSession;
import org.nuxeo.ecm.core.api.DocumentModel;
import org.nuxeo.ecm.core.api.blobholder.BlobHolder;
import org.nuxeo.ecm.core.api.impl.blob.FileBlob;
import org.nuxeo.ecm.core.event.Event;
import org.nuxeo.ecm.core.event.EventBundle;
import org.nuxeo.ecm.core.event.EventContext;
import org.nuxeo.ecm.core.event.PostCommitEventListener;
import org.nuxeo.ecm.core.event.impl.DocumentEventContext;
import org.nuxeo.runtime.api.Framework;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Set;

public class ForumListener implements PostCommitEventListener{

	private static final Log log = LogFactory.getLog(ForumListener.class);

	private static final int THRESHOLD = 1024 * 1024;

	@Override
	public void handleEvent(EventBundle events) {
		for (Event event : events) {
			handleEvent(event);
		}
	}
	
	protected void handleEvent(Event event) {
		EventContext eventContext = event.getContext();
		if (!(eventContext instanceof DocumentEventContext)) {
			return;
		}
		
		DocumentEventContext documentEventContext = (DocumentEventContext) eventContext;
		DocumentModel documentModel = documentEventContext.getSourceDocument();
		if (!documentModel.hasFacet("ForumProgram") || documentModel.isProxy()) {
			return;
		}
		
		CoreSession session = documentModel.getCoreSession();
		AutomationService automationService = Framework.getService(AutomationService.class);

		OperationContext operationContext = new OperationContext();

		ForumProgramMaker forumProgramMaker = new ForumProgramMaker(session, documentModel.getTitle());
		String forumProgramDescription = "";
		for (Section section : forumProgramMaker.getForumProgram().getSections()) {
			forumProgramDescription += section.getTitle() + "\n";
			forumProgramDescription += section.getDescription() + "\n";
			forumProgramDescription += "----------------------\n\n";
		}

		File file = generatePDFForumProgram(forumProgramMaker.getForumProgram(), documentModel);

		Blob blob = new FileBlob(file);
		BlobHolder blobHolder = documentModel.getAdapter(BlobHolder.class);
		blobHolder.setBlob(blob);

		documentModel = session.saveDocument(documentModel);
		operationContext.setInput(documentModel);
		operationContext.setCoreSession(session);
		OperationChain operationChain = new OperationChain("ForumProgramListenerChain");
		operationChain.add("javascript.ForumProgramDefaultMapper");

		try {
			automationService.run(operationContext, operationChain);
		} catch (OperationException e) {
			e.printStackTrace();
		}
		file.delete();
	}

	private File generatePDFForumProgram(ForumProgram forumProgram, DocumentModel documentModel) {
		Document document = new Document(PageSize.A4, 50, 50, 50, 50);
		String FILE_NAME = "/tmp/" + forumProgram.getTitle() + ".pdf";
		File file = new File(FILE_NAME);
		try {
			PdfWriter.getInstance(document, new FileOutputStream(file));
			//open
			document.open();

			//add metadata
			document.addTitle(documentModel.getTitle());
			document.addAuthor(documentModel.getPropertyValue("dc:creator").toString());
			document.addCreator(documentModel.getPropertyValue("dc:creator").toString());

			BaseFont baseFont = BaseFont.createFont("fonts/arial.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			Font font = new Font(baseFont, 12);
			Set<Section> sections = forumProgram.getSections();
			for (Section section : sections) {
				font.setStyle(Font.BOLD);
				font.setSize(14);
				document.add(new Paragraph(section.getTitle(), font));
				log.warn("Paragraph: " + section.getTitle());
				font.setStyle(Font.NORMAL);
				font.setSize(12);
				document.add(new Paragraph(section.getDescription(), font));
				log.warn("Paragraph: " + section.getDescription());
				document.add(new Paragraph("\n\r"));
			}
			document.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return file;
	}
}